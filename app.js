'use strict'

require('dotenv').config()

const express = require('express')
const basicAuth = require('express-basic-auth')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const swaggerUi = require('swagger-ui-express')
const cors = require('cors')
const app = express()
const port = 3001

const routers = require('./routes')
const errorHandler = require('./handler/error')
const middlewareHandler = require('./middleware/handler')
const swaggerFile = require('./swagger-output.json')

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')

  next()
})
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

middlewareHandler.init(app)
app.use(routers)
app.use('/docs', basicAuth({ users: { 'admin@nikaah.id': 'Ayonikaah123.' }, challenge: true, }), swaggerUi.serve, swaggerUi.setup(swaggerFile))
errorHandler(app)

app.listen(port, () => { console.log('service has succees started ') })

module.exports = app
