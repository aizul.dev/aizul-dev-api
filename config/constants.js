'use strict'

module.exports = {
    ERROR: {
        failed_create_event: 'Terjadi kesalahan saat menambahkan acara, coba sekali lagi'
    },
    Dayname : {
        'sun': 'Minggu',
        'mon': 'Senin',
        'tue': 'Selasa',
        'wed': 'Rabu',
        'thu': 'Kamis',
        'fri': 'Jum\'at',
        'sat': 'Sabtu'
    },
    DaynameEn : {
        'sun': 'Sunday',
        'mon': 'Monday',
        'tue': 'Tuesday',
        'wed': 'Wednesday',
        'thu': 'Thursday',
        'fri': 'Friday',
        'sat': 'Saturday'
    },
    Monthname : {
        'jan': 'Januari',
        'feb': 'Februari',
        'mar': 'Maret',
        'apr': 'April',
        'mei': 'Maret',
        'jun': 'Juni',
        'jul': 'Juli',
        'aug': 'Agustus',
        'sep': 'September',
        'oct': 'Oktober',
        'nov': 'November',
        'dec': 'Desember'
    },
    based_url : {
        'local': 'http://localhost:3000',
        'development': null,
        'production': 'https://api-nikaah.freeweb.id'
    },
    ui_based_url: {
        local: 'https://nikaah-client.vercel.app',
        development: 'https://nikaah-client.vercel.app',
        production: 'https://client.nikaah.id'
    },
    bucket: {
        'BASE_URL': process.env.BUCKET_BASE_URL,
        'HOST': process.env.BUCKET_HOST,
        'NAME': process.env.BUCKET_NAME,
        'PORT': parseInt(process.env.BUCKET_PORT),
        'ACCESS_KEY': process.env.BUCKET_ACCESS_KEY,
        'SECRET_KEY': process.env.BUCKET_SECRET_KEY,
    },
    smtp_service: {
        API_KEY: process.env.SENDINBLUE_KEY,
        SENDER_NAME: process.env.SENDER_NAME,
        SENDER_EMAIL: process.env.SENDER_EMAIL
    },
    is_maintenance: process.env.MAINTENANCE,
    env: process.env.NODE_ENV,
    user_private_key: '1f134408-d5e0-4515-bab7-fad8d8b13c47d',
    admin_private_key: 'd63b01a3-d284-42a2-aaa7-dc2aaf2b9677'
}