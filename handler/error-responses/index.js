'use strict'

/**
 * Module dependencies.
 * @private
 */

const {
  errorFormResponse,
  errorModuleResponse,
  errorTypeResponse,
  errorUnknownResponse
} = require('./responses')

function customBodyParserError (error) {
  if (['entity.parse.failed'].includes(error.type)) {
    return {
      status: 400,
      errorType: 'invalid_body',
      errorMessage: 'Format body salah'
    }
  }

  if (error instanceof URIError) {
    return {
      status: 400,
      errorType: 'invalid_uri',
      errorMessage: 'URI tidak valid'
    }
  }

  return null
}

function getErrorResponse (err) {
  if (!err.errorForm && !err.errorType) {
    const customErr = customBodyParserError(err)
    if (customErr) {
      return errorModuleResponse(customErr.errorType, customErr.errorMessage, { status: customErr.status })
    }

    return errorUnknownResponse(err)
  }

  if (err.errorForm) {
    return errorFormResponse(err.errorType, err.errorForm, { message: err.ErrorMessage })
  }

  const args = err.errorArgs || []
  return errorTypeResponse(err.errorType, { args: args, data: err.errorData })
}

module.exports = {
  getErrorResponse
}
