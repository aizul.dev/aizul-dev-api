'use strict'

module.exports = function (type, message, { status = 400 }) {
  return {
    status: status,
    type: type,
    message: message
  }
}
