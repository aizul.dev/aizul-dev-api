'use strict'

const errorFormResponse = require('./errorFormResponse')
const errorModuleResponse = require('./errorModuleResponse')
const errorTypeResponse = require('./errorTypeResponse')
const errorUnknownResponse = require('./errorUnknownResponse')

module.exports = {
  errorFormResponse,
  errorModuleResponse,
  errorTypeResponse,
  errorUnknownResponse
}
