'use strict'

const _ = require('lodash')
const ServiceError = require('../../handler/wrapper/ServiceError')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)

class Tool {
  constructor () {
    this.ProvRepo = instanceModels.master_provinces
    this.CityRepo = instanceModels.master_cities
  }

  async getProv (whereClause = null) {
    const provs = await this.ProvRepo.findAll({
      where: whereClause,
      attributes: [
        ['prov_id', 'id'],
        ['prov_name', 'name']
      ]
    })
    return { message: 'Berhasil mendapatkan daftar provinsi', data: provs }
  }

  async getCity (provId, cityId = null) {
    const whereClause = { prov_id: provId }
    if (cityId) whereClause.city_id = cityId

    const cities = await this.CityRepo.findAll({
      where: whereClause,
      attributes: [
        ['city_id', 'id'],
        ['city_name', 'name']
      ]
    })
    return { message: 'Berhasil mendapatkan daftar kota/kabupaten', data: cities }
  }

  getSource () {
    return {
      message: 'Berhasil mendapatkan daftar sumber informasi',
      data: [
        {
          id: 1,
          name: 'Teman/Keluarga'
        },
        {
          id: 2,
          name: 'Instagram'
        },
        {
          id: 3,
          name: 'Facebook'
        },
        {
          id: 4,
          name: 'Google'
        },
        {
          id: 0,
          name: 'Lainnya'
        }
      ]
    }
  }
}

module.exports = new Tool()