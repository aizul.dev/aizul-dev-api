'use strict'

const moment = require('moment')
const spliter = require('../helper/tokenSpliter')
const Token = require('../lib/auth/token')
const User = require('../lib/user')
const ERR_FAIL_ACCESS = 'token incorrect'
const ALLOWED_TOKEN_TYPE = ['Bearer']
const JWT_KEY = require('../config/app_config.json')['user_private_key']

module.exports = async (req, res, next) => {
    if (req.userdata) return next()

    const Authorization = req.get('Authorization')
    const SplitAuthorization = spliter(Authorization)

    if (SplitAuthorization.length !== 2) return res.status(401).json({ message: ERR_FAIL_ACCESS })

    const [type, value] = SplitAuthorization
    if (ALLOWED_TOKEN_TYPE.indexOf(type) < 0) { return res.status(401).json({ message: ERR_FAIL_ACCESS }) }

    const verify = Token.verify(value ,JWT_KEY)
    const now = moment().unix()

    if (!verify) {
        return res.status(401).json({ message: ERR_FAIL_ACCESS })
    }

    const { exp: expAt, user } = verify

    if (now > expAt) {
        return res.status(401).json({ message: 'token expired' })
    }

    try {
        const { data: userdata } = await User.getProfile(user.id, true)
        const { status } = userdata

        if (status < 1) return res.status(401).json({ message: 'Your account is not active' })
        req.userdata = userdata
        return next()
    } catch (error) {
        console.log(error)
        return res.status(401).json({ message: 'User access denied' })
    }
}