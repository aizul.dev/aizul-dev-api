'use strict'

const { validationMiddleware } = require('./util')

module.exports = validationMiddleware({
  Tool: require('./tool')
})
