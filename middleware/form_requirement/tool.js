'use strict'

const { header, body } = require('express-validator')
const moment = require('moment')

const Tool = require('../../lib/tool/index')
const { hasUppercase, hasLowercase, hasNumeric} = require('../../helper/containsCustom')
const { map } = require('lodash')
const availableRegion = ['62']

module.exports = {
  complete_register: [
    body('code')
      .exists().withMessage('Kode region tidak boleh kosong!')
      .not().isEmpty().withMessage('Kode region harus diisi!')
      .custom(val => {
        if (!availableRegion.includes(val)) return Promise.reject('Kode region tersebut belum didukung')
        return true
      })
  ]
}
