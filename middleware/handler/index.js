'use strict'

const { is_maintenance } = require('../../config/constants')

module.exports.init = app => {
  app.use((req, res, next) => {
    console.info({ url: req.originalUrl })

    if (is_maintenance && is_maintenance == 1) {
      console.info('MAINTENANCE MODE')
      return res.status(503).json({
        message: 'Maintenance is under way. Bibit will be back alive and kicking soon'
      })
    } else {
      next()
    }
  })
}
