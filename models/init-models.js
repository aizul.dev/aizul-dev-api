var DataTypes = require("sequelize").DataTypes;
var _master_cities = require("./masterCities");
var _master_provinces = require("./masterProvinces");

function initModels(sequelize) {
  var master_cities = _master_cities(sequelize, DataTypes);
  var master_provinces = _master_provinces(sequelize, DataTypes);


  return {
    master_cities,
    master_provinces,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
