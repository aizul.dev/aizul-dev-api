'use strict'

const express = require('express')
const router = express.Router()

router.use('/tools', require('./tool'))

module.exports = router
